﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Data;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SuperPanels
{
    public partial class MainPage : PhoneApplicationPage
    {

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }

            getHighScore();
        }

        //writing data 
        //private async Task WriteToFile(string folder, string txtFile, string text)
        //{
        //    byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(text.ToCharArray());
        //
        //    StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
        //
        //    var dataFolder = await local.CreateFolderAsync(@"Data\" + folder,
        //        CreationCollisionOption.OpenIfExists);
        //
        //    var file = await dataFolder.CreateFileAsync(txtFile + ".txt",
        //        CreationCollisionOption.ReplaceExisting);
        //
        //    using (var s = await file.OpenStreamForWriteAsync())
        //    {
        //        s.Write(fileBytes, 0, fileBytes.Length);
        //    }
        //}
        
        //reading data
        //private async Task ReadFile(string folder, string txtFile)
        //{
        //    StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
        //
        //    if (local != null)
        //    {
        //        var dataFolder = await local.GetFolderAsync(@"Data\" + folder);
        //
        //        var file = await dataFolder.OpenStreamForReadAsync(txtFile + ".txt");
        //
        //        using (StreamReader streamReader = new StreamReader(file))
        //        {
        //            string ret = streamReader.ReadToEnd();
        //            highscore = ret;
        //            highscoreText.Text = "Highscore: " + highscore;
        //        }
        //
        //    }
        //}

        //private async void getHighScore();
        private void getHighScore()
        {
            //await this.ReadFile("scores", "highscore");

            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if( !settings.Contains("highscore_easy") )
            {
                writeHighScore();
            }

            string highscore_easy = settings["highscore_easy"] as string;
            highScoreText_Easy.Text = "Highscore: " + highscore_easy;

            if (!settings.Contains("highscore_medium"))
            {
                writeHighScore();
            }

            string highscore_medium = settings["highscore_medium"] as string;
            highScoreText_Medium.Text = "Highscore: " + highscore_medium;

            if (!settings.Contains("highscore_hard"))
            {
                writeHighScore();
            }

            string highscore_hard = settings["highscore_hard"] as string;
            highScoreText_Hard.Text = "Highscore: " + highscore_hard;
        }

        //private async void writeHighScore()
        private void writeHighScore()
        {
            //write high score if needed
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if(!settings.Contains("highscore_easy"))
            {
                settings.Add("highscore_easy", "0");
            }
            if (!settings.Contains("highscore_medium"))
            {
                settings.Add("highscore_medium", "0");
            }
            if (!settings.Contains("highscore_hard"))
            {
                settings.Add("highscore_hard", "0");
            }
            settings.Save();

            //StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
            //
            //var dataFolder = await local.CreateFolderAsync(@"Data\scores",
            //    CreationCollisionOption.OpenIfExists);
            //
            //var file = await dataFolder.CreateFileAsync("highscore.txt",
            //    CreationCollisionOption.ReplaceExisting);
            //
            //if (file == null)
            //{
            //    await this.WriteToFile("scores", "highscore", "0");
            //}
        }

        private void TextBlock_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string tileText = "1";
            NavigationService.Navigate(new Uri(string.Format("/gamePage.xaml?difficulty={0}", Uri.EscapeDataString(tileText)), UriKind.Relative));
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            string tileText = "1";
            NavigationService.Navigate(new Uri(string.Format("/gamePage.xaml?difficulty={0}", Uri.EscapeDataString(tileText)), UriKind.Relative));
        }

        private void StackPanel_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string tileText = "0";
            NavigationService.Navigate(new Uri(string.Format("/gamePage.xaml?difficulty={0}", Uri.EscapeDataString(tileText)), UriKind.Relative));
        }

        private void StackPanel_Tap_2(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string tileText = "1";
            NavigationService.Navigate(new Uri(string.Format("/gamePage.xaml?difficulty={0}", Uri.EscapeDataString(tileText)), UriKind.Relative));
        }

        private void StackPanel_Tap_3(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string tileText = "2";
            NavigationService.Navigate(new Uri(string.Format("/gamePage.xaml?difficulty={0}", Uri.EscapeDataString(tileText)), UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            string tileText = "3";
            NavigationService.Navigate(new Uri(string.Format("/gamePage.xaml?difficulty={0}", Uri.EscapeDataString(tileText)), UriKind.Relative));
        }

    }
}
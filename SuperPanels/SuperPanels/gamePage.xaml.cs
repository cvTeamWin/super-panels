﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Media;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Data;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SuperPanels
{
    public class gridItemObject
    {
        public int health;
        public string color;
        public static Image image;

        public void doSomething()
        {
            //do nothing
        }

        public Image addImage(Image umage)
        {
            gridItemObject.image = umage;
            return umage;
        }

        public Image calculateColors(Image umage)
        {
            gridItemObject umage_ctr = (gridItemObject)umage.Tag;

            if (umage_ctr.health >= 150)
            {
                umage.Source = new BitmapImage(new Uri("Assets/images/hundred/PurplePanelEasy.png", UriKind.Relative));
                umage_ctr.color = "Purple";
            }
            else if (umage_ctr.health >= 125 && umage_ctr.health < 150)
            {
                umage.Source = new BitmapImage(new Uri("Assets/images/hundred/BluePanelEasy.png", UriKind.Relative));
                umage_ctr.color = "Blue";
            }
            else if (umage_ctr.health < 125 && umage_ctr.health > 75)
            {
                umage.Source = new BitmapImage(new Uri("Assets/images/hundred/GreenPanelEasy.png", UriKind.Relative));
                umage_ctr.color = "Green";
            }
            else if (umage_ctr.health <= 75 && umage_ctr.health > 50)
            {
                umage.Source = new BitmapImage(new Uri("Assets/images/hundred/YellowPanelEasy.png", UriKind.Relative));
                umage_ctr.color = "Yellow";
            }
            else if (umage_ctr.health <= 50 && umage_ctr.health > 25)
            {
                umage.Source = new BitmapImage(new Uri("Assets/images/hundred/OrangePanelEasy.png", UriKind.Relative));
                umage_ctr.color = "Orange";
            }
            else if (umage_ctr.health <= 25)
            {
                umage.Source = new BitmapImage(new Uri("Assets/images/hundred/RedPanelEasy.png", UriKind.Relative));
                umage_ctr.color = "Red";
            }
            else
            {
                umage.Source = new BitmapImage(new Uri("Assets/ApplicationIcon.png", UriKind.Relative));
                umage_ctr.color = "White";
            }

            return umage;
        }
    }


    public partial class gamePage : PhoneApplicationPage
    {
        string tiles;
        int tileNum;
        string highScore;
        //public static Image health;

        //database stuff
        //database handling
        //private async Task WriteToFile(string folder, string txtFile, string text)
        //{
        //    byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(text.ToCharArray());
        //
        //    StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
        //
        //    var dataFolder = await local.CreateFolderAsync(@"Data\" + folder,
        //        CreationCollisionOption.OpenIfExists);
        //
        //    var file = await dataFolder.CreateFileAsync(txtFile + ".txt",
        //        CreationCollisionOption.ReplaceExisting);
        //
        //    using (var s = await file.OpenStreamForWriteAsync())
        //    {
        //        s.Write(fileBytes, 0, fileBytes.Length);
        //    }
        //}
        //private async Task ReadFile(string folder, string txtFile)
        //{
        //    StorageFolder local = Windows.Storage.ApplicationData.Current.LocalFolder;
        //
        //    if (local != null)
        //    {
        //        var dataFolder = await local.GetFolderAsync(@"Data\" + folder);
        //
        //        var file = await dataFolder.OpenStreamForReadAsync(txtFile + ".txt");
        //
        //        using (StreamReader streamReader = new StreamReader(file))
        //        {
        //            string ret = streamReader.ReadToEnd();
        //            highScore = ret;
        //        }
        //
        //    }
        //}


        //Function to get random number
        private static readonly Random getrandom = new Random();
        private static readonly object syncLock = new object();
        public static int GetRandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return getrandom.Next(min, max);
            }
        }

        public gamePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            tiles = NavigationContext.QueryString["difficulty"]; //0 = easy, 1 = medium, 2 = hard

            SystemTray.IsVisible = false;

            //get high score for use later
            getHighScore();
        }

        //private async void getHighScore()
        string diff;
        private void getHighScore()
        {
            //await this.ReadFile("scores", "highscore");
            int tilenum = Convert.ToInt32(tiles);
            switch (tilenum)
            {
                case 0:
                    diff = "easy";
                    break;
                case 1:
                    diff = "medium";
                    break;
                case 2:
                    diff = "hard";
                    break;
                case 3:
                    diff = "easy";
                    break;
                default:
                    diff = "hard";
                    break;
            }

            if(IsolatedStorageSettings.ApplicationSettings.Contains("highscore_" + diff))
            {
                highScore = IsolatedStorageSettings.ApplicationSettings["highscore_" + diff] as string;

                highScoreTextBlock.Text = "High Score: " + highScore;
            }
            else
            {
                writeHighScore("0");
            }
        }
        
        //private async void writeHighScore(string score)
        private void writeHighScore(string score)
        {
            //await this.WriteToFile("scores", "highscore", score);
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            if(!settings.Contains("highscore_" + diff))
            {
                settings.Add("highscore_" + diff, score);
            }
            else
            {
                settings["highscore_" + diff] = score;
            }
            settings.Save();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Would you like to exit the game?", "Paused",
                                   MessageBoxButton.OKCancel) != MessageBoxResult.OK)
            {
                e.Cancel = true;

            }

            base.OnBackKeyPress(e);
        }

        //public Image healthChange(Image umage, int amount)
        //{
        //    gridItemObject umage_ctr = (gridItemObject)umage.Tag;
        //
        //    TextBlock healthChangeBlock = new TextBlock();
        //    healthChangeBlock.Text = amount.ToString();
        //    healthChangeBlock.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
        //    healthChangeBlock.Margin = umage.Margin;
        //    mainGrid.Children.Add(healthChangeBlock);
        //
        //    return umage;
        //}



        private void startGameBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {


            string tile = tiles;
            string tileStr = tile.ToString();
            tileNum = Convert.ToInt32(tile);
            int sizeNum = Convert.ToInt32(tile);
            double fStr = 0;
            double tStr = 0;
            switch (tileNum)
            {
                case 0:
                    tileNum = 6;
                    break;
                case 1:
                    tileNum = 10;
                    break;
                case 2:
                    tileNum = 32;
                    break;
                case 3:
                    tileNum = 1;
                    break;
                default:
                    tileNum = 19;
                    break;
            }

            for (int i = 0; i < tileNum; i++)
            {
                string a = i.ToString(); //this will be used for counting grid items but haven't figured that out yet
                gridItemObject gridItemOBJ = new gridItemObject();
                Image gridItem = new Image();
                gridItemOBJ.addImage(gridItem);
                gridItem.Tag = gridItemOBJ;
                gridItem.Name += "gridItemCount_" + i;
                gridItemOBJ.health = 100;
                gridItem.HorizontalAlignment = HorizontalAlignment.Left;
                gridItem.VerticalAlignment = VerticalAlignment.Top;
                switch (sizeNum)
                {
                    case 0:
                        gridItem.Height = 180;
                        gridItem.Width = 180;
                        //System.Diagnostics.Debug.WriteLine("easy");
                        fStr = 80 + (230 * i);
                        tStr = 10;
                        if (i > 2)
                        {
                            tStr = 210;
                            fStr = 80 + (230 * (i - 3));
                        }
                        gridItem.Margin = new Thickness(fStr, tStr, 0, 0);
                        gridItem.VerticalAlignment = VerticalAlignment.Top;
                        break;
                    case 1:
                        gridItem.Height = 125;
                        gridItem.Width = 125;
                        //System.Diagnostics.Debug.WriteLine("medium");
                        fStr = 65 + (145 * i);
                        tStr = 60;
                        if (i > 4 && i < 10)
                        {
                            fStr = 65 + (145 * (i - 5));
                            tStr = 210;
                        }
                        gridItem.Margin = new Thickness(fStr, tStr, 0, 0);
                        gridItem.VerticalAlignment = VerticalAlignment.Top;
                        break;
                    case 2:
                        gridItem.Height = 85;
                        gridItem.Width = 85;
                        //System.Diagnostics.Debug.WriteLine("hard");
                        fStr = 30 + (95 * i);
                        tStr = 30;
                        if (i > 7 && i <= 15)
                        {
                            fStr = 30 + (95 * (i - 8));
                            tStr = 125;
                        }
                        else if (i > 15 && i <= 23)
                        {
                            fStr = 30 + (95 * (i - 16));
                            tStr = 220;
                        }
                        else if (i > 23 && i < 32)
                        {
                            fStr = 30 + (95 * (i - 24));
                            tStr = 315;
                        }
                        gridItem.Margin = new Thickness(fStr, tStr, 0, 0);
                        gridItem.VerticalAlignment = VerticalAlignment.Top;
                        break;
                    case 3:
                        gridItem.Height = 200;
                        gridItem.Width = 200;
                        //System.Diagnostics.Debug.WriteLine("poo");
                        fStr = 200 + (85 * i);
                        tStr = 30;
                        gridItem.Margin = new Thickness(fStr, tStr, 0, 0);
                        gridItem.VerticalAlignment = VerticalAlignment.Top;
                        break;
                    default:
                        gridItem.Height = 100;
                        gridItem.Width = 100;
                        //System.Diagnostics.Debug.WriteLine("default");
                        break;
                }
                gridItem.Tap += gridItem_tap;
                gridItemOBJ.calculateColors(gridItem);
                mainGrid.Children.Add(gridItem);
            }

            mainGrid.Children.Remove(startGameBlock);
            mainGrid.Children.Remove(multitouchAlert);
            StartTimer();


            Button Rejuvenation = new Button();
            Rejuvenation.HorizontalAlignment = HorizontalAlignment.Left;
            Rejuvenation.VerticalAlignment = VerticalAlignment.Center;
            Rejuvenation.Height = 50;
            Rejuvenation.Width = 500;
            Rejuvenation.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
            Rejuvenation.Content = "Heal";
            Rejuvenation.Tap += Rejuvenation_Tap;
            //LayoutRoot.Children.Add(Rejuvenation);
        }

        int cd = 0;

        private void Rejuvenation_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            cd = 1;
            //throw new NotImplementedException();
        }

        private void gridItem_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string tile = tiles;
            string tileStr = tile.ToString();

         //  for (int i = 0; i < tileNum; i++); 
            int difficulty = Convert.ToInt32(tiles);
            Image umage = (Image)sender;

            Image glow = new Image();
            glow.Source = new BitmapImage(new Uri("Assets/glowEffect.png", UriKind.Relative));
            glow.Margin = umage.Margin;
            glow.Height = umage.Height;
            glow.Width = umage.Width;
            glow.HorizontalAlignment = umage.HorizontalAlignment;
            glow.VerticalAlignment = umage.VerticalAlignment;
            mainGrid.Children.Add(glow);

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(33);
            timer.Tick += new EventHandler((sendertwo, ex) => takeGlowAwayTick(timer, e, glow));
            timer.Start();
       //   TimeSpan span = new TimeSpan(0, 0, 0, 2, 0);
       //   Console.WriteLine(span);
       //   mainGrid.Children.Remove(glow);

            gridItemObject umage_ctr = (gridItemObject)umage.Tag;
            if (umage_ctr.health >= 150)
            {
                //do nothing
            }
            else
            {
                switch (difficulty)
                {
                    case 0:
                        umage_ctr.health += 25;
                        break;
                    case 1:
                        umage_ctr.health += 25;
                        break;
                    case 2:
                        umage_ctr.health += 25;
                        break;
                    case 3:
                        umage_ctr.health += 50;
                        break;
                    default:
                        umage_ctr.health += 5;
                        break;
                }
            }
            umage_ctr.calculateColors(umage);
            //throw new NotImplementedException();
        }

        private object takeGlowAwayTick(object sender, EventArgs e, Image glow)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Stop();

            mainGrid.Children.Remove(glow);

            return sender;
            //throw new NotImplementedException();
        }

        double timerTime = 1500;

        System.Windows.Threading.DispatcherTimer myDispatchTimer = new System.Windows.Threading.DispatcherTimer();

        public void StartTimer()
        {

            myDispatchTimer.Interval = TimeSpan.FromMilliseconds(100); // initial 100 milisecond wait... really nothing
            myDispatchTimer.Tick += new EventHandler(Initial_Wait);
            myDispatchTimer.Start();
        }

        void Initial_Wait(object o, EventArgs sender)
        {
            // Stop the timer, replace the tick handler, and restart with new interval.
            myDispatchTimer.Stop();
            myDispatchTimer.Tick -= new EventHandler(Initial_Wait);
            myDispatchTimer.Interval = TimeSpan.FromMilliseconds(timerTime); //every x seconds
            myDispatchTimer.Tick += new EventHandler(Each_Tick);
            myDispatchTimer.Start();
        }

        int q = 0;

        // Ticker


        void Each_Tick(object o, EventArgs sender)
        {
            timerTime -= 10;

            if(timerTime <= 0)
            {
                timerTime = 10;
            }

            myDispatchTimer.Interval = TimeSpan.FromMilliseconds(timerTime);

            int difficulty = Convert.ToInt32(tiles);
            switch (difficulty)
            {
                case 0:
                    {
                        if(q > Convert.ToInt32(highScore))
                        {
                            titleBlock.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                        }

                        titleBlock.Text = "Score: " + q++.ToString();
                        //timerTime -= 10;

                        for (int w = 0; w < tileNum; w++)
                        {
                            int r = GetRandomNumber(0, 3);
                            if (cd == 1)
                            {
                                r = 2;
                            }
                            if (r == 0)
                            {
                                continue;
                            }
                            else
                            {
                                Image umage = (Image)mainGrid.FindName("gridItemCount_" + w);
                                gridItemObject umage_ctr = (gridItemObject)umage.Tag;
                                if (cd == 1)
                                {
                                    umage_ctr.health += 25;
                                }
                                else
                                {
                                umage_ctr.health -= 25;
                                }

                                umage_ctr.calculateColors(umage);

                                if (umage_ctr.health == 0) // they just lost! crash the app for now
                                {
                                    //Application.Current.Terminate();
                                    onLoose("lost");
                                }
                            }
                        }
                        break;
                    }
                //----------------------------------------------------------------------------------------------------------------------------------                  
                case 1:
                    {
                        if (q > Convert.ToInt32(highScore))
                        {
                            titleBlock.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                        }

                        titleBlock.Text = "Score: " + q++.ToString();
                        //timerTime -= 10;

                        for (int w = 0; w < tileNum; w++)
                        {
                            int r = GetRandomNumber(0, 3);

                            if (r == 0)
                            {
                                continue;
                            }
                            else
                            {
                                Image umage = (Image)mainGrid.FindName("gridItemCount_" + w);
                                gridItemObject umage_ctr = (gridItemObject)umage.Tag;
                                umage_ctr.health -= 25;

                                umage_ctr.calculateColors(umage);

                                if (umage_ctr.health == 0) // they just lost! crash the app for now
                                {
                                    //Application.Current.Terminate();
                                    onLoose("lost");
                                }
                            }
                        }
                        break;
                    }
                //------------------------------------------------------------------------------------------------------------------------------------
                case 2:
                    {
                        if (q > Convert.ToInt32(highScore))
                        {
                            titleBlock.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                        }

                        titleBlock.Text = "Score: " + q++.ToString();
                        //timerTime -= 10;

                        for (int w = 0; w < tileNum; w++)
                        {
                            int r = GetRandomNumber(0, 8);

                            if (r <= 2)
                            {
                                Image umage = (Image)mainGrid.FindName("gridItemCount_" + w);
                                gridItemObject umage_ctr = (gridItemObject)umage.Tag;
                                umage_ctr.health -= 25;

                                umage_ctr.calculateColors(umage);

                                if (umage_ctr.health == 0) // they just lost! crash the app for now
                                {
                                    //Application.Current.Terminate();
                                    onLoose("lost");
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        break;
                    }
                    //-------------------------------------------------------------------------------------------------------------
                    case 3:
                    {
                        if (q > Convert.ToInt32(highScore))
                        {
                            titleBlock.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                        }

                        titleBlock.Text = "Score: " + q++.ToString();
                        //timerTime -= 10;

                        for (int w = 0; w < tileNum; w++)
                        {
                            int r = GetRandomNumber(1, 2);

                            if (r == 0)
                            {
                                continue;
                            }
                            else
                            {
                                Image umage = (Image)mainGrid.FindName("gridItemCount_" + w);
                                gridItemObject umage_ctr = (gridItemObject)umage.Tag;
                                umage_ctr.health -= 50;

                                umage_ctr.calculateColors(umage);

                                if (umage_ctr.health == 0) // they just lost! crash the app for now
                                {
                                    //Application.Current.Terminate();
                                    onLoose("lost");
                                }
                            }

                        }
                        break;
                    }
            }

        }

        bool newHighScore;

        void onLoose(string a)
        {
            myDispatchTimer.Stop();
            handleScore( q );

            Image Backdrop = new Image();
            Backdrop.Source = new BitmapImage(new Uri("Assets/PanoramaBackground.PNG", UriKind.Relative));
            Backdrop.HorizontalAlignment = HorizontalAlignment.Center;
            Backdrop.Height = 1800;
            Backdrop.Width = 2400;
            Backdrop.VerticalAlignment = VerticalAlignment.Top;
            Backdrop.Margin = new Thickness(0, 0, 0, 0);
            LayoutRoot.Children.Add(Backdrop);

            TextBlock endgame = new TextBlock();
            endgame.HorizontalAlignment = HorizontalAlignment.Center;
            //endgame.Margin = new Thickness(10, 40, 0, 0);
            endgame.Margin = new Thickness(0, 40, 0, 0);
            endgame.Text = "GAME OVER";
            endgame.VerticalAlignment = VerticalAlignment.Top;
            endgame.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
            endgame.FontSize = 70;
          //endgame.Tap += endgame_Tap;
            LayoutRoot.Children.Add(endgame);

            TextBlock scoreText = new TextBlock();
            scoreText.HorizontalAlignment = HorizontalAlignment.Center;
            scoreText.Margin = new Thickness(0, 120, 0, 0);
            scoreText.Text = "Score: " + q;
            scoreText.VerticalAlignment = VerticalAlignment.Top;
            scoreText.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
            scoreText.FontSize = 40;
            LayoutRoot.Children.Add(scoreText);


            TextBlock newHighScoreText = new TextBlock();
            newHighScoreText.HorizontalAlignment = HorizontalAlignment.Center;
            newHighScoreText.Margin = new Thickness(0, 180, 0, 0);

            if(newHighScore == true)
            {
                newHighScoreText.Text = "NEW HIGH SCORE!!";
            }
            else
            {
                newHighScoreText.Text = "High Score: " + highScore;
            }

            newHighScoreText.VerticalAlignment = VerticalAlignment.Top;
            newHighScoreText.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
            newHighScoreText.FontSize = 40;
            LayoutRoot.Children.Add(newHighScoreText);


            Button back = new Button();
            back.HorizontalAlignment = HorizontalAlignment.Center;
            back.VerticalAlignment = VerticalAlignment.Top;
            back.Height = 100;
            back.Width = 300;
            //back.Margin = new Thickness(-10, 250, 0, 0);
            back.Margin = new Thickness(0, 250, 0, 0);
            back.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
            back.Content = "Back to menu";
            back.Tap += back_Tap;
            LayoutRoot.Children.Add(back);


            Button wuit = new Button();
            wuit.HorizontalAlignment = HorizontalAlignment.Center;
            wuit.VerticalAlignment = VerticalAlignment.Top;
            wuit.Height = 100;
            wuit.Width = 300;
            //wuit.Margin = new Thickness(-10, 350, 0, 0);
            wuit.Margin = new Thickness(0, 350, 0, 0);
            wuit.FontFamily = Application.Current.Resources["PhoneFontFamilyBold"] as FontFamily;
            wuit.Content = "Exit Game";
            wuit.Tap += wuit_Tap;
            LayoutRoot.Children.Add(wuit);
        }

        void wuit_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //throw new NotImplementedException();
            Application.Current.Terminate();
        }

        void back_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
          //throw new NotImplementedException();
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

        void handleScore(int score)
        {
            //convert high score to int
            int high;
            bool result = Int32.TryParse(highScore, out high);

            if(result == false)
            {
                high = 0;
            }

            //compare the two
            if (score > high) //higher highscore!!
            {
                newHighScore = true;
                //so lets write new high score
                highScore = Convert.ToString(score);

                writeHighScore(highScore);
            }
            else
            {
                newHighScore = false;
            }
        }

        public EventArgs h { get; set; }
    }
}

